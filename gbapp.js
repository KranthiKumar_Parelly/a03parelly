var express = require("express");
var app     = express();
var path    = require("path");
var logger = require("morgan");
var http = require('http').Server(app);
var bodyParser = require("body-parser");
app.use(express.static(__dirname + '/assets'));
app.use(express.static(__dirname + '/views'));

// set up the logger
app.use(logger("dev"));
app.use(bodyParser.urlencoded({ extended: false }));
app.set("views", path.resolve(__dirname, "views")); // path to views
app.set("view engine", "ejs"); // specify our view engine


// manage our entries
var entries = [];
app.locals.entries = entries;

  //__dirname : It will resolve to your project folder.

// http GET (default and /new-entry)
app.get("/", function (request, response) {
  response.render("home");  
});
app.get("/index", function (request, response) {
  response.render("home");  
});
app.get("/Contact", function (request, response) {
  response.render("Contact");  
});
app.get("/guestbook", function (request, response) {
  response.render("gb");  
});
app.get("/JS", function (request, response) {
  response.render("JavaScript");  
});
app.get("/new-entry", function (request, response) { 
  response.render("new-entry");
});
app.get("/test", function (request, response) { 
  response.render("test");
});
// http POST (INSERT)
// mail
app.post("/Contact",function(req,res){
var api_key = 'key-0eb3f22c6f839af0145214690f4dcd3b';
var domain = 'sandbox1577adc49a4d40bca9bf7e5166e2f465.mailgun.org';
var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
 
var data = {
  from: 'A03 User <postmaster@sandbox1577adc49a4d40bca9bf7e5166e2f465.mailgun.org>',
  to: 'kranthiparelly@gmail.com',
  subject: req.body.UserName,
  text: req.body.message
};
 
mailgun.messages().send(data, function (error, body) {
  console.log(body);
  if(!error)
  res.send("Mail Sent");
  else
  res.send("Mail not sent")
});
})



app.post("/new-entry", function (request, response) {
  if (!request.body.title || !request.body.body) {
    response.status(400).send("Entries must have a title and a body.");
    return;
  }
  entries.push({
    title: request.body.title,
    content: request.body.body,
    published: new Date()
  });
  response.redirect("home");
});
app.use(function (request, response) {
  response.status(200).render("home");
});
// 404
app.use(function (request, response) {
  response.status(404).render("404");
});

app.listen(8081, function () {
  console.log("My Website on http://127.0.0.1:8081/");
});